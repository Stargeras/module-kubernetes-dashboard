resource "helm_release" "dashboard" {
  name             = "kubernetes-dashboard"
  namespace        = "kubernetes-dashboard"
  create_namespace = "true"
  repository       = "https://kubernetes.github.io/dashboard"
  chart            = "kubernetes-dashboard"

  values = [
    templatefile("${path.module}/values-tmpl.yaml", {
      ingress_domain = var.ingress_domain
    })
  ]
}
