module "dashboard" {
  source             = "./modules/dashboard"
  ingress_domain     = var.ingress_domain
}