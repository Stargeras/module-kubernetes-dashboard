# Sourcing example
``` terraform
module "kubernetes-dashboard" {
  source                                 = "git::https://gitlab.com/Stargeras/module-kubernetes-dashboard"
  ingress_domain                         = "example.com"
  kubernetes_auth_host                   = module.kubernetes.kubernetes_auth_host
  kubernetes_auth_cluster_ca_certificate = module.kubernetes.kubernetes_auth_cluster_ca_certificate
  kubernetes_auth_client_certificate     = module.kubernetes.kubernetes_auth_client_certificate
  kubernetes_auth_client_key             = module.kubernetes.kubernetes_auth_client_key
  kubernetes_auth_token                  = module.kubernetes.kubernetes_auth_token
}
```
